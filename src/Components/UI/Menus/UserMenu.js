import React, {Fragment} from 'react';
import {MenuItem, Nav, NavDropdown} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserMenu = ({user, logout}) => {
  const navTitle = (
    <Fragment>
      Hello, <b>{user.username}!</b>
    </Fragment>
  );

  return (
    <Nav pullRight>
      <NavDropdown title={navTitle} id="user-menu">
        <LinkContainer to='/artists/add_new' exact>
          <MenuItem>
            Add new artist
          </MenuItem>
        </LinkContainer>
        <LinkContainer to='/albums/add_new' exact>
          <MenuItem>
            Add new album
          </MenuItem>
        </LinkContainer>
        <LinkContainer to='/tracks/add_new' exact>
          <MenuItem>
            Add new track
          </MenuItem>
        </LinkContainer>
        <LinkContainer to={`/track_history?user=${user._id}`} exact>
          <MenuItem>
            Show my track history
          </MenuItem>
        </LinkContainer>
        <MenuItem divider/>
        <MenuItem onClick={logout}>Logout</MenuItem>
      </NavDropdown>
    </Nav>
  )
};

export default UserMenu;