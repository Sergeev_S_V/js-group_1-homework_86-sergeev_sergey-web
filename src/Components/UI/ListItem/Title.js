import React from 'react';

const Title = props => (
  <p>{props.title}{props.number && '#' + props.number}:
    <span> {props.name}</span>
  </p>
);

export default Title;
