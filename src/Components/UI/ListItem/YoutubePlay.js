import React, {Fragment} from 'react';
import YouTube from 'react-youtube';

const YoutubePlay = props => {
  return(
    <Fragment>
      <span onClick={props.play} style={{fontWeight: 'bold'}}>
        Play
      </span>
        {props.youtubeKey
          && <YouTube opts={props.opts}
                      videoId={props.youtubeKey}
                      onReady={props.onReady}/>}
    </Fragment>
  )
};

export default YoutubePlay;