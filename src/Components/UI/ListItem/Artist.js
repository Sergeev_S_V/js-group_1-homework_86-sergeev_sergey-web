import React from 'react';

const WithArtist = props => (
  <span>Artist: {props.artist}</span>
);

export default WithArtist;