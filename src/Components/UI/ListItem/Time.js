import React from 'react';

const Time = props => (
  <span>{props.type}: <span>{props.time} </span></span>
);

export default Time;