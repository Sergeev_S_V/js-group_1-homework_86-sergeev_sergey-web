import React from 'react';
import {Button, Panel} from "react-bootstrap";
import ImageItem from "../ListItem/ImageItem";
import YoutubePlay from "../ListItem/YoutubePlay";
import Artist from "../ListItem/Artist";
import Time from "../ListItem/Time";
import Title from "../ListItem/Title";

const ListItem = props => {
  return <Panel>
      <Panel.Heading onClick={props.clicked} style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <Title title={props.title} number={props.number} name={props.name}/>
      </Panel.Heading>
      <Panel.Body style={{display: 'flex', justifyContent: 'flex-start'}}>
        {props.image && <ImageItem image={props.image}/>}
        {props.artist && <Artist artist={props.artist}/>}
        {props.info && <p>{props.info}</p>}
      </Panel.Body>
      <Panel.Footer style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        {props.type && props.time && <Time type={props.type} time={props.time}/>}
        {props.play && <YoutubePlay play={props.play}
                                    opts={props.opts}
                                    youtubeKey={props.youtubeKey}
                                    onReady={props.onReady}/>
        }
        {props.user
          && <Button disabled={!!props.published}
                     onClick={props.toPublish} bsStyle="primary">
                     {props.published ? 'Published' : 'Publish'}
             </Button>
        }
        {props.user
          && <span><b>{props.published ? 'Published' : 'Unpublished'}</b></span>
        }
        {props.canDelete &&
          <Button onClick={props.delete}>Delete</Button>
        }
      </Panel.Footer>
  </Panel>
};


export default ListItem;