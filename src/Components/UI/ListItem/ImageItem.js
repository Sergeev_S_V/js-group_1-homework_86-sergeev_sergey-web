import React from 'react';
import {Image} from "react-bootstrap";

const ImageItem = props => (
  <Image style={{width: '200px', height: '150px'}} src={props.image} thumbnail/>
);

export default ImageItem;