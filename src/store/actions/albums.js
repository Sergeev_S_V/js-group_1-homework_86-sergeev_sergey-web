import axios from '../../axios-api';
import {
  FAILURE_FETCH_ALBUMS, PUBLISH_ALBUM_SUCCESS, SUCCESS_ADD_NEW_ALBUM, SUCCESS_DELETE_ALBUM, SUCCESS_DELETE_ARTIST,
  SUCCESS_FETCH_ALBUMS
} from "./actionTypes";
import {push} from "react-router-redux";

const successFetchAlbums = albums => {
  return {type: SUCCESS_FETCH_ALBUMS, albums};
};

const failureFetchAlbums = error => {
  return {type: FAILURE_FETCH_ALBUMS, error};
};

export const fetchAlbums = artistId => async dispatch => {
  if (artistId) {
    try {
      let albums = await axios.get(`/albums?artist=${artistId}`);
      dispatch(successFetchAlbums(albums.data));
    } catch (e) {
      dispatch(failureFetchAlbums(e));
    }
  } else {
    try {
      let albums = await axios.get(`/albums`);
      dispatch(successFetchAlbums(albums.data));
    } catch (e) {
      dispatch(failureFetchAlbums(e));
    }
  }
};

export const addNewAlbum = (album, token) => async dispatch => {
  const headers = {"Token": token};

  await axios.post(`/albums`, album, {headers});
  dispatch(successAddNewAlbum());
  dispatch(push('/'));
};

const successAddNewAlbum = () => ({
  type: SUCCESS_ADD_NEW_ALBUM
});

export const publishAlbum = (albumId, artistId, token) => async dispatch => {
  const headers = {"Token": token};

  if (artistId) {
    console.log(artistId);
    const updatedAlbums = await axios.post(`/albums/${albumId}/publish`, {artistId}, {headers});
    dispatch(publishAlbumSuccess(updatedAlbums.data));
  } else {
    const updatedAlbums = await axios.post(`/albums/${albumId}/publish`, null, {headers});
    dispatch(publishAlbumSuccess(updatedAlbums.data));
  }

};

const publishAlbumSuccess = (updatedAlbums) => {
  return {type: PUBLISH_ALBUM_SUCCESS, updatedAlbums}
};

export const deleteAlbum = (albumId, token) => async dispatch => {
  const headers = {"Token": token};

  const updatedAlbums = await axios.delete(`/albums/${albumId}`, {headers});
  dispatch(successDeleteAlbum(updatedAlbums.data));
};

const successDeleteAlbum = updatedAlbums => {
  return {type: SUCCESS_DELETE_ALBUM, updatedAlbums};
};