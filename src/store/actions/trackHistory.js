import axios from '../../axios-api';
import {SUCCESS_FETCH_TRACK_HISTORY} from "./actionTypes";

const successFetchTrackHistory = trackHistory => {
  return {type: SUCCESS_FETCH_TRACK_HISTORY, trackHistory};
};

export const fetchTrackHistory = (token) => dispatch => {
  const headers = {"Token": token};

  axios.get(`/track_history`, {headers})
    .then(res => {
      res && dispatch(successFetchTrackHistory(res.data));
    });
};