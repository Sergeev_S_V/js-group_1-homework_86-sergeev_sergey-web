import axios from '../../axios-api';
import {
  FAILURE_FETCH_TRACKS, PUBLISH_TRACK_SUCCESS, SUCCESS_ADD_NEW_TRACK, SUCCESS_DELETE_ALBUM, SUCCESS_DELETE_TRACK,
  SUCCESS_FETCH_TRACKS,
  SUCCESS_GET_YOUTUBE_LINK
} from "./actionTypes";
import {push} from "react-router-redux";

const successFetchTracks = tracks => ({
  type: SUCCESS_FETCH_TRACKS, tracks
});

const failureFetchTracks = error => ({
  type: FAILURE_FETCH_TRACKS, error
});

export const fetchTracks = albumId => async dispatch => {
  if (albumId) {
    try {
      let tracks = await axios.get(`/tracks?album=${albumId}`);
      dispatch(successFetchTracks(tracks.data));
    } catch (e) {
      dispatch(failureFetchTracks(e));
    }
  } else {
    try {
      let tracks = await axios.get(`/tracks`);
      dispatch(successFetchTracks(tracks.data));
    } catch (e) {
      dispatch(failureFetchTracks(e));
    }
  }

};

export const addInTrackHistory = (trackId, token) => dispatch => {
  axios.post('/track_history', {trackId}, {headers:{"Token": token}});
};

export const playOnYoutube = trackId => dispatch => {
  axios.get(`/tracks?track=${trackId}`)
    .then(res => dispatch(successGetYoutubeLink(res.data)));
};

export const successGetYoutubeLink = link => {
  return {type: SUCCESS_GET_YOUTUBE_LINK, link};
};

export const addNewTrack = (track, token) => async dispatch => {
  const headers = {"Token": token};

  await axios.post(`/tracks`, track, {headers});
  dispatch(successAddNewTrack());
  dispatch(push('/'));
};

const successAddNewTrack = () => ({
  type: SUCCESS_ADD_NEW_TRACK
});

export const publishTrack = (albumId, trackId, token) => async dispatch => {
  const headers = {"Token": token};
  if (albumId) {
    const updatedTracks = await axios.post(`/tracks/${trackId}/publish`, {albumId}, {headers});
    dispatch(publishTrackSuccess(updatedTracks.data));
  } else {
    const updatedTracks = await axios.post(`/tracks/${trackId}/publish`, null, {headers});
    dispatch(publishTrackSuccess(updatedTracks.data));
  }

};

const publishTrackSuccess = (updatedTracks) => {
  return {type: PUBLISH_TRACK_SUCCESS, updatedTracks}
};

export const deleteTrack = (albumId, trackId, token) => async dispatch => {
  const headers = {"Token": token};
  if (albumId) {
    await axios.delete(`/tracks/${trackId}`, {headers});
    dispatch(fetchTracks(albumId));
  } else {
    const updatedTracks = await axios.delete(`/tracks/${trackId}`, {headers});
    dispatch(successDeleteTrack(updatedTracks.data));
  }
};

const successDeleteTrack = updatedTracks => {
  return {type: SUCCESS_DELETE_TRACK, updatedTracks};
};

