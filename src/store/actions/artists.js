import axios from '../../axios-api';
import {
  FAILURE_FETCH_ARTISTS, PUBLISH_ARTIST_SUCCESS, SUCCESS_ADD_NEW_ARTIST, SUCCESS_DELETE_ARTIST,
  SUCCESS_FETCH_ARTISTS
} from "./actionTypes";
import {push} from "react-router-redux";


const successFetchArtists = artists => {
  return {type: SUCCESS_FETCH_ARTISTS, artists};
};

const failureFetchArtists = error => {
  return {type: FAILURE_FETCH_ARTISTS, error};
};

export const fetchArtists = () => dispatch => {
  axios.get('/artists').then(
    res => {
      res && dispatch(successFetchArtists(res.data));
    }, err => {
      err && dispatch(failureFetchArtists(err));
    })
};

export const addNewArtist = (artist, token) => async dispatch => {
  const headers = {"Token": token};

  await axios.post(`/artists`, artist, {headers});
  dispatch(successAddNewArtist());
  dispatch(push('/'));
};

const successAddNewArtist = () => ({
  type: SUCCESS_ADD_NEW_ARTIST
});

export const publishArtist = (artistId, token) => async dispatch => {
  const headers = {"Token": token};

  const updatedArtists = await axios.post(`/artists/${artistId}/publish`, null, {headers});
  dispatch(publishArtistSuccess(updatedArtists.data));
};

const publishArtistSuccess = (updatedArtists) => ({
  type: PUBLISH_ARTIST_SUCCESS, updatedArtists
});

export const deleteArtist = (artistId, token) => async dispatch => {
  const headers = {"Token": token};

  const updatedArtists = await axios.delete(`/artists/${artistId}`, {headers});
  dispatch(successDeleteArtist(updatedArtists.data));
};

const successDeleteArtist = updatedArtists => {
  return {type: SUCCESS_DELETE_ARTIST, updatedArtists};
};