import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import {routerMiddleware, routerReducer} from 'react-router-redux';

import {loadState, saveState} from "./localStorage";

import albumsReducer from '../store/reducers/albums';
import artistsReducer from '../store/reducers/artists';
import tracksReducer from '../store/reducers/tracks';
import usersReducer from '../store/reducers/users';
import trackHistoryReducer from '../store/reducers/trackHistory';

const rootReducer = combineReducers({
  albums: albumsReducer,
  tracks: tracksReducer,
  artists: artistsReducer,
  users: usersReducer,
  trackHistory: trackHistoryReducer,
  routing: routerReducer
});

export const history = createHistory();

const middleware = [
  thunkMiddleware,
  routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
  saveState({
    users: store.getState().users
  });
});

export default store;