import {SUCCESS_FETCH_TRACK_HISTORY} from "../actions/actionTypes";

const initialState = {
  trackHistory: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_FETCH_TRACK_HISTORY:
      return {...state, trackHistory: action.trackHistory};
    default:
      return state;
  }
};

export default reducer;