import {
  PUBLISH_TRACK_SUCCESS, SUCCESS_DELETE_TRACK, SUCCESS_FETCH_TRACKS,
  SUCCESS_GET_YOUTUBE_LINK
} from "../actions/actionTypes";

const initialState = {
  tracks: [],
  youtubeLink: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_FETCH_TRACKS:
      return {...state, tracks: action.tracks};
    case SUCCESS_GET_YOUTUBE_LINK:
      return {...state, youtubeLink: action.link};
    case PUBLISH_TRACK_SUCCESS:
      return {...state, tracks: action.updatedTracks};
    case SUCCESS_DELETE_TRACK:
      return {...state, tracks: action.updatedTracks};
    default:
      return state;
  }
};

export default reducer;