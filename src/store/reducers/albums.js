import {PUBLISH_ALBUM_SUCCESS, SUCCESS_DELETE_ALBUM, SUCCESS_FETCH_ALBUMS} from "../actions/actionTypes";

const initialState = {
  albums: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_FETCH_ALBUMS:
      return {...state, albums: action.albums};
    case PUBLISH_ALBUM_SUCCESS:
      return {...state, albums: action.updatedAlbums};
    case SUCCESS_DELETE_ALBUM:
      return {...state, albums: action.updatedAlbums};
    default:
      return state;
  }
};

export default reducer;