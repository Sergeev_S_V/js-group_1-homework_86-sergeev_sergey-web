import {PUBLISH_ARTIST_SUCCESS, SUCCESS_DELETE_ARTIST, SUCCESS_FETCH_ARTISTS} from "../actions/actionTypes";

const initialState = {
  artists: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_FETCH_ARTISTS:
      return {...state, artists: action.artists};
    case PUBLISH_ARTIST_SUCCESS:
      return {...state, artists: action.updatedArtists};
    case SUCCESS_DELETE_ARTIST:
      return {...state, artists: action.updatedArtists};
    default:
      return state;
  }
};

export default reducer;