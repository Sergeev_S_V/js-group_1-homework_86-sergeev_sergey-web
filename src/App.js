import React, { Component } from 'react';
import Layout from "./Components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import ArtistsList from "./Containers/ArtistsList/ArtistsList";
import AlbumsList from "./Containers/AlbumsList/AlbumsList";
import TracksList from "./Containers/TracksList/TracksList";
import Register from "./Containers/Register/Register";
import Login from "./Containers/Login/Login";
import TrackHistory from "./Containers/TrackHistory/TrackHistory";
import AddNewArtist from "./Containers/AddNewArtist/AddNewArtist";
import AddNewAlbum from "./Containers/AddNewAlbum/AddNewAlbum";
import AddNewTrack from "./Containers/AddNewTrack/AddNewTrack";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path='/' exact component={ArtistsList}/>
          <Route path='/artists' exact component={ArtistsList}/>
          <Route path='/albums' exact component={AlbumsList}/>
          <Route path='/tracks' exact component={TracksList}/>
          <Route path='/register' exact component={Register}/>
          <Route path='/login' exact component={Login}/>
          <Route path='/track_history' exact component={TrackHistory}/>
          <Route path='/artists/add_new' exact component={AddNewArtist}/>
          <Route path='/albums/add_new' exact component={AddNewAlbum}/>
          <Route path='/tracks/add_new' exact component={AddNewTrack}/>
        </Switch>
      </Layout>
    );
  }
}

// artists - получать всех артистов с сервера
// albums - получать все альбомы с сервера
// tracks - получать все трэки с сервера
// навести красоту
// если не добавлять image она все равно добавляется

export default App;
