import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../Components/UI/FormElement/FormElement";
import {connect} from "react-redux";
import {addNewAlbum} from "../../store/actions/albums";
import {fetchArtists} from "../../store/actions/artists";

class AddNewAlbum extends Component {

  state = {
    title: '',
    image: '',
    artist: '',
    releaseDate: ''
  };

  componentDidMount() {
    this.props.onFetchArtists();
  }

  onSubmitHandler = event => {
    event.preventDefault();

    if (this.state.title.length && this.state.artist.length && this.state.releaseDate.length > 0) {
      const token = this.props.user.token;

      let formData = new FormData();
      Object.keys(this.state).forEach(key => (
        formData.append(key, this.state[key])
      ));

      this.props.onAddNewAlbum(formData, token);
      this.setState({title: '', image: '', artist: '', releaseDate: ''});
    }
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name]: event.target.files[0]});
  };

  render() {
    return(
      <Fragment>
        <PageHeader>
          Add new album
        </PageHeader>
        <Form horizontal onSubmit={this.onSubmitHandler}>
          <FormElement title='Album'
                       type='text'
                       placeholder='Album'
                       required
                       value={this.state.title}
                       changeHandler={this.inputChangeHandler}
                       propertyName='title'
          />

          <FormElement title='Release date'
                       type='text'
                       required
                       placeholder='Release date'
                       value={this.state.releaseDate}
                       changeHandler={this.inputChangeHandler}
                       propertyName='releaseDate'

          />

          <FormElement title='Choose artist'
                       type='select'
                       placeholder='Choose artist'
                       changeHandler={this.inputChangeHandler}
                       propertyName='artist'
                       required
                       options={this.props.artists}
                       value={this.state.artist}
          />

          <FormElement title='Image'
                       type='file'
                       placeholder='Image'
                       changeHandler={this.fileChangeHandler}
                       propertyName='image'

          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">
                Add new album
              </Button>
            </Col>
          </FormGroup>

        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
  onAddNewAlbum: (album, token) => dispatch(addNewAlbum(album, token)),
  onFetchArtists: () => dispatch(fetchArtists())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewAlbum);