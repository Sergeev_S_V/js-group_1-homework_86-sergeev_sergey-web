import React, {Component, Fragment} from 'react';
import {PageHeader} from "react-bootstrap";
import {connect} from "react-redux";
import {fetchTrackHistory} from "../../store/actions/trackHistory";
import ListItem from "../../Components/UI/ListItem/ListItem";

class TrackHistory extends Component {

  componentDidMount() {
    if (this.props.user === null) {
      this.props.history.push('/login');

    } else {
      const token = this.props.user.token;

      this.props.fetchTrackHistory(token);
    }
  };

  sortTrackHistory = trackHistory => trackHistory.sort((a, b) => new Date(a.datetime) - new Date(b.datetime));

  renderTrackHistory = trackHistory => {
    let sortedTrackHistory = this.sortTrackHistory(trackHistory).reverse();

    return sortedTrackHistory.map(trackHistory => (
      <ListItem key={trackHistory._id}
                title='Track'
                name={trackHistory.trackId.title}
                number={trackHistory.trackId.number}
                type='Date'
                time={trackHistory.datetime}
                artist={trackHistory.trackId.album.artist.name}/>
    ));
  };

  render() {
    return (
      <Fragment>
        <PageHeader>
          Track history
        </PageHeader>
        {this.props.trackHistory.length > 0
          ? this.renderTrackHistory(this.props.trackHistory)
          : <div>List is empty</div>
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  trackHistory: state.trackHistory.trackHistory,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  fetchTrackHistory: (token) => dispatch(fetchTrackHistory(token))
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);