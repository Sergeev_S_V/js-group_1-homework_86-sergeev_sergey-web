import React, {Component, Fragment} from 'react';
import {PageHeader} from "react-bootstrap";
import ListItem from "../../Components/UI/ListItem/ListItem";
import notFound from '../../assets/images/not-found.png';
import {deleteArtist, fetchArtists, publishArtist} from "../../store/actions/artists";
import {connect} from "react-redux";
import config from '../../config';

class ArtistsList extends Component {

  componentDidMount() {
    this.props.onFetchArtists();
  }

  goToLink = (artistId) => {
    this.props.history.push(`/albums?artist=${artistId}`);
  };

  onPublishArtistHandler = artistId => {
    const token = this.props.user.token;

    this.props.onPublishArtist(artistId, token);
  };

  onDeleteArtistHandler = (artistId) => {
    const token = this.props.user.token;
    
    this.props.onDeleteArtist(artistId, token);
  };

  renderForAnonymous = artists => {
    let image = notFound;

    return artists.map(artist => {
      if (artist.published) {

        if (artist.image) {
          image = config.apiUrl + '/uploads/' + artist.image;
        }

        return <ListItem key={artist._id}
                         clicked={() => this.goToLink(artist._id)}
                         image={image}
                         title='Artist'
                         info={artist.information}
                         name={artist.name}
        />
      }

    });
  };

  renderForUsersArtists = artists => {
    const user = this.props.user;

    let canDelete = user.role === 'admin';

    return artists.map(artist => {
      let image = notFound;
      if (artist.userId === user._id || user.role === 'admin') {

        if (artist.image) {
          image = config.apiUrl + '/uploads/' + artist.image;
        }

        return <ListItem key={artist._id}
                         clicked={() => this.goToLink(artist._id)}
                         image={image}
                         title='Artist'
                         info={artist.information}
                         name={artist.name}
                         user
                         published={artist.published}
                         toPublish={() => this.onPublishArtistHandler(artist._id)}
                         canDelete={canDelete}
                         delete={() => this.onDeleteArtistHandler(artist._id)}
        />
      }

    });

  };

  render() {
    return(
      <Fragment>
        <PageHeader>
          Artists List
        </PageHeader>
        {this.props.artists.length > 0 && this.props.user
          && this.renderForUsersArtists(this.props.artists)
        }
        {this.props.artists.length > 0 && !this.props.user
          && this.renderForAnonymous(this.props.artists)
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  artists: state.artists.artists,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchArtists: () => dispatch(fetchArtists()),
  onPublishArtist: (artistId, token) => dispatch(publishArtist(artistId, token)),
  onDeleteArtist: (artistId, token) => dispatch(deleteArtist(artistId, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsList);