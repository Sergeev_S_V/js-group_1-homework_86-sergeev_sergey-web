import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../Components/UI/FormElement/FormElement";
import {connect} from "react-redux";
import {addNewArtist} from "../../store/actions/artists";

class AddNewArtist extends Component {

  state = {
    name: '',
    image: '',
    information: ''
  };

  onSubmitHandler = event => {
    event.preventDefault();

    if (this.state.name.length > 0) {
      const token = this.props.user.token;

      let formData = new FormData();
      Object.keys(this.state).forEach(key => (
        formData.append(key, this.state[key])
      ));

      this.props.onAddNewArtist(formData, token);
      this.setState({name: '', image: '', information: ''});
    }
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name]: event.target.files[0]});
  };

  render() {
    return(
      <Fragment>
        <PageHeader>
          Add new artist
        </PageHeader>
        <Form horizontal onSubmit={this.onSubmitHandler}>
          <FormElement title='Artist'
                       type='text'
                       placeholder='Name'
                       required
                       value={this.state.name}
                       changeHandler={this.inputChangeHandler}
                       propertyName='name'

          />

          <FormElement title='Information'
                       type='textarea'
                       placeholder='Information'
                       value={this.state.information}
                       changeHandler={this.inputChangeHandler}
                       propertyName='information'

          />

          <FormElement title='Image'
                       type='file'
                       placeholder='Image'
                       changeHandler={this.fileChangeHandler}
                       propertyName='image'

          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">
                Add new artist
              </Button>
            </Col>
          </FormGroup>

        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onAddNewArtist: (artist, token) => dispatch(addNewArtist(artist, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewArtist);