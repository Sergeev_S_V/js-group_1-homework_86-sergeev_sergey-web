import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../Components/UI/FormElement/FormElement";
import {connect} from "react-redux";
import {fetchArtists} from "../../store/actions/artists";
import {addNewTrack} from "../../store/actions/tracks";
import {fetchAlbums} from "../../store/actions/albums";

class AddNewTrack extends Component {

  state = {
    number: '',
    title: '',
    album: '',
    duration: '',
    youtubeLink: '',
    artist: '',
  };

  componentDidMount() {
    this.props.onFetchArtists();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.artist.length > 0) {
      if (this.state.artist !== prevState.artist) {
        this.props.onFetchAlbums(this.state.artist);
      }
    }
  }

  onSubmitHandler = event => {
    event.preventDefault();
    if (this.state.title.length && this.state.album.length && this.state.duration.length > 0) {
      const token = this.props.user.token;

      const track = {
        number: this.state.number,
        title: this.state.title,
        album: this.state.album,
        duration: this.state.duration,
        youtubeLink: this.state.youtubeLink,
      };

      this.props.onAddNewTrack(track, token);
      this.setState({number: '', title: '', album: '', duration: '', youtubeLink: '', artist: ''});
    }
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  render() {
    console.log(this.props.artists);
    console.log(this.props.albums);
    return(
      <Fragment>
        <PageHeader>
          Add new track
        </PageHeader>
        <Form horizontal onSubmit={this.onSubmitHandler}>

          <FormElement title='Number'
                       type='number'
                       required
                       placeholder='Number'
                       value={this.state.number}
                       changeHandler={this.inputChangeHandler}
                       propertyName='number'

          />

          <FormElement title='Track'
                       type='text'
                       placeholder='Track'
                       required
                       value={this.state.title}
                       changeHandler={this.inputChangeHandler}
                       propertyName='title'
          />

          <FormElement title='Duration'
                       type='text'
                       required
                       placeholder='Duration'
                       value={this.state.duration}
                       changeHandler={this.inputChangeHandler}
                       propertyName='duration'

          />

          <FormElement title='Youtube link'
                       type='text'
                       placeholder='Youtube link'
                       value={this.state.youtubeLink}
                       changeHandler={this.inputChangeHandler}
                       propertyName='youtubeLink'

          />

          <FormElement title='Choose artist'
                       type='select'
                       placeholder='Choose artist'
                       changeHandler={this.inputChangeHandler}
                       propertyName='artist'
                       required
                       options={this.props.artists}
                       value={this.state.artist}
          />

          <FormElement title='Choose album'
                       type='select'
                       placeholder='Choose album'
                       changeHandler={this.inputChangeHandler}
                       propertyName='album'
                       required
                       options={this.props.albums}
                       value={this.state.album}
          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">
                Add new track
              </Button>
            </Col>
          </FormGroup>

        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  albums: state.albums.albums,
  artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
  onAddNewTrack: (track, token) => dispatch(addNewTrack(track, token)),
  onFetchArtists: () => dispatch(fetchArtists()),
  onFetchAlbums: artistId => dispatch(fetchAlbums(artistId))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewTrack);