import React, {Component, Fragment} from 'react';
import {PageHeader} from "react-bootstrap";
import ListItem from "../../Components/UI/ListItem/ListItem";
import {addInTrackHistory, deleteTrack, fetchTracks, playOnYoutube, publishTrack} from "../../store/actions/tracks";
import {connect} from "react-redux";
import queryString from 'query-string';
import config from "../../config";

const opts = {
  height: '300',
  width: '500',
};

class TracksList extends Component {

  componentDidMount() {
    const albumId = this.getAlbumId(this.props.location.search);
    this.props.onFetchTracks(albumId);
  }

  parseQueryString = data => {
    return queryString.parse(data);
  };

  getAlbumId = data => {
    let parsedData = this.parseQueryString(data);
    return parsedData.album;
  };

  sortTracks = tracks => tracks.sort((a, b) => a.number > b.number);

  onReady = event => event.target.playVideo();

  getYoutubeKey = link => {
    let youtubeKey;
    let params = new URLSearchParams(link);
    for (let value of params.entries()) {
      youtubeKey = value[1];
    }
    return youtubeKey;
  };

  onPublishTrackHandler = (albumId, trackId) => {
    const token = this.props.user.token;

    this.props.onPublishTrack(albumId, trackId, token);
  };

  onDeleteTrackHandler = (albumId, trackId) => {
    const token = this.props.user.token;

    this.props.onDeleteTrack(albumId, trackId, token);
  };

  renderForAnonymous = tracks => {
    let sortedTracks = this.sortTracks(tracks);

    return sortedTracks.map(track => {

      if (track.published) {
        let youtubeKey = this.getYoutubeKey(this.props.link);

        return <ListItem key={track._id}
                         title='Track'
                         name={track.title}
                         number={track.number}
                         type='Duration'
                         time={track.duration}
                         play={() => this.props.playOnYoutube(track._id)}
                         opts={opts}
                         youtubeKey={youtubeKey}
                         onReady={(event) => this.onReady(event)}
                         published={track.published}

        />
      }

    });
  };

  renderTracksForUsers = tracks => {
    const user = this.props.user;

    let canDelete = user.role === 'admin' && true;
    let sortedTracks = this.sortTracks(tracks);

    return sortedTracks.map(track => {
      if (track.userId === user._id || user.role === 'admin') {

        let youtubeKey = this.getYoutubeKey(this.props.link);

        return <ListItem key={track._id}
                         title='Track'
                         name={track.title}
                         number={track.number}
                         type='Duration'
                         time={track.duration}
                         play={() => this.props.playOnYoutube(track._id)}
                         opts={opts}
                         youtubeKey={youtubeKey}
                         onReady={(event) => this.onReady(event)}
                         clicked={() => this.onAddInTrackHistoryHandler(track._id)}
                         user
                         published={track.published}
                         toPublish={() => this.onPublishTrackHandler(track.album._id, track._id)}
                         canDelete={canDelete}
                         delete={() => this.onDeleteTrackHandler(track.album._id, track._id)}
        />
      }
    });
  };

  onAddInTrackHistoryHandler = trackId => {
    if (this.props.user === null) {
      this.props.history.push('/login');
    } else {
      const token = this.props.user.token;
      this.props.addInTrackHistory(trackId, token);
    }
  };

  render() {
    return(
      <Fragment>
        <PageHeader>
          Tracks List
        </PageHeader>
        {this.props.tracks.length > 0 && this.props.user
          && this.renderTracksForUsers(this.props.tracks)
        }
        {this.props.tracks.length > 0 && !this.props.user
          && this.renderForAnonymous(this.props.tracks)
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  tracks: state.tracks.tracks,
  user: state.users.user,
  link: state.tracks.youtubeLink
});

const mapDispatchToProps = dispatch => ({
  onFetchTracks: (albumId) => dispatch(fetchTracks(albumId)),
  onDeleteTrack: (albumId, trackId, token) => dispatch(deleteTrack(albumId, trackId, token)),
  onPublishTrack: (albumId, trackId, token) => dispatch(publishTrack(albumId, trackId, token)),
  addInTrackHistory: (trackId, token) => dispatch(addInTrackHistory(trackId, token)),
  playOnYoutube: trackId => dispatch(playOnYoutube(trackId))
});

export default connect(mapStateToProps, mapDispatchToProps)(TracksList);