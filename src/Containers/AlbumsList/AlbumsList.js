import React, {Component, Fragment} from 'react';
import queryString from 'query-string';
import {connect} from "react-redux";
import {deleteAlbum, fetchAlbums, publishAlbum} from "../../store/actions/albums";
import {PageHeader} from "react-bootstrap";
import ListItem from "../../Components/UI/ListItem/ListItem";
import notFound from '../../assets/images/not-found.png';
import config from "../../config";


class AlbumsList extends Component {

  componentDidMount() {
    const artistId = this.getArtistId(this.props.location.search);
    this.props.onFetchAlbums(artistId);
  };

  parseQueryString = data => {
    return queryString.parse(data);
  };

  getArtistId = data => {
    let parsedData = this.parseQueryString(data);
    return parsedData.artist;
  };

  goToLink = (albumId) => {
    this.props.history.push(`/tracks?album=${albumId}`);
  };

  onPublishAlbumHandler = (albumId, artistId) => {
    const token = this.props.user.token;

    this.props.onPublishAlbum(albumId, artistId, token);
  };

  onDeleteAlbumHandler = (albumId) => {
    const token = this.props.user.token;

    this.props.onDeleteAlbum(albumId, token);
  };

  renderForAnonymous = albums => {
    let image = notFound;
    let releaseDate = 'Unknown';

    return albums.map(album => {
      if (album.published) {

        if (album.image) {
          image = config.apiUrl + '/uploads/' + album.image;
        }
        if (album.releaseDate) {
          releaseDate = album.releaseDate;
        }

        return <ListItem key={album._id}
                         clicked={() => this.goToLink(album._id)}
                         image={image}
                         title='Album'
                         type='Release date'
                         name={album.title}
                         time={releaseDate}
                         published={album.published}
        />
      }

    });
  };

  renderAlbumsForUsers = albums => {
    const user = this.props.user;

    let releaseDate = 'Unknown';
    let image = notFound;
    let canDelete = user.role === 'admin' && true;

    return albums.map(album => {
      if (album.userId === user._id || user.role === 'admin') {

        if (album.releaseDate) {
          releaseDate = album.releaseDate;
        }
        if (album.image) {
          image = config.apiUrl + '/uploads/' + album.image;
        }

        return <ListItem key={album._id}
                         title='Album'
                         type='Release date'
                         name={album.title}
                         image={image}
                         time={releaseDate}
                         clicked={() => this.goToLink(album._id)}
                         user
                         published={album.published}
                         toPublish={() => this.onPublishAlbumHandler(album._id, album.artist._id)}
                         canDelete={canDelete}
                         delete={() => this.onDeleteAlbumHandler(album._id)}
        />
      }

    })
  };

  render() {
    return(
      <Fragment>
        <PageHeader>
          Albums List
        </PageHeader>
        {this.props.albums.length > 0 && this.props.user
          && this.renderAlbumsForUsers(this.props.albums)
        }
        {this.props.albums.length > 0 && !this.props.user
          && this.renderForAnonymous(this.props.albums)
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  albums: state.albums.albums,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchAlbums: artistId => dispatch(fetchAlbums(artistId)),
  onPublishAlbum: (albumId, artistId, token) => dispatch(publishAlbum(albumId, artistId, token)),
  onDeleteAlbum: (albumId, token) => dispatch(deleteAlbum(albumId, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumsList);