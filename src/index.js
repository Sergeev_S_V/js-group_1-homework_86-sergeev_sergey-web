import React from 'react';
import {Provider} from 'react-redux';
import ReactDOM from 'react-dom';
import {ConnectedRouter} from 'react-router-redux';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import {history} from './store/configureStore';
import store from './store/configureStore';

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
